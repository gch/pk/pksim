#ifndef RANDOM_H
#define RANDOM_H 1

#include "types.h"

struct prng_squares_ctx
{
	uint64_t seed;
	uint64_t key;
	uint64_t ctr;
};

extern void rand_init(void);
extern void prng_squares_init(struct prng_squares_ctx *ctx);
extern void prng_squares_init_seed(struct prng_squares_ctx *ctx, uint64_t seed);
extern void prng_squares_init_key_ctr(struct prng_squares_ctx *ctx, uint64_t key, uint64_t ctr);
extern uint32_t prng_squares(struct prng_squares_ctx *ctx);
extern float randf_norm_unsafe(void);
extern float randf_snorm_unsafe(void);
extern v2_t rand_normal_masaglia_unsafe(void);
extern float rand_normal_unsafe(void);
extern v2_t v2_rand_normal_unsafe(void);
extern v4_t v4_rand_normal_unsafe(void);
extern v6_t v6_rand_normal_unsafe(void);

#endif /* RANDOM_H */
