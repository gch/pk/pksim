#include <math.h>
#include <stdlib.h>

#include "stats.h"
#include "types.h"
#include "util.h"

static int cmp_float_cb(const void *pa, const void *pb)
{
	float a = *(float *)pa;
	float b = *(float *)pb;

	return (a > b) - (a < b);
}

void stats_samples_sort_asc(size_t n, float xs[])
{
	qsort(xs, n, sizeof(xs[0]), cmp_float_cb);
}

/* Needs a sorted array as input */
float stats_quantile_r7(float p, size_t n, const float xs[])
{
	if (n == 0)
		return NAN;

	p = clampf(0.f, p, 1.f);
	float h = (n - 1) * p;
	size_t hfi = (size_t)h;
	float hf = (float)hfi;
	float xf = xs[hfi];

	/* floor(h) == h <=> floor(h) == ceil(h) <=> xf == xc */
	if (hf == h)
		return xf;

	size_t hci = hfi + 1;
	float xc = xs[hci];
	return xf + (h - hf) * (xc - xf);
}
