#ifndef UTIL_H
#define UTIL_H 1

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static inline float min2f(float a, float b) {
	return a < b ? a : b;
}

static inline size_t min2sz(size_t a, size_t b) {
	return a < b ? a : b;
}

static inline float max2f(float a, float b) {
	return a > b ? a : b;
}

static inline size_t max2sz(size_t a, size_t b) {
	return a > b ? a : b;
}

static inline float clampf(float l, float a, float h) {
	return min2f(max2f(l, a), h);
}

static inline size_t clampsz(size_t l, size_t a, size_t h) {
	return min2sz(max2sz(l, a), h);
}

static inline char *str_dup(const char *str) {
	size_t size = strlen(str) + 1;
	char *s = malloc(size);
	if (!s)
		return NULL;

	memcpy(s, str, size);
	return s;
}

static inline void *free_p(void *pptr) {
	void **p = pptr;
	if (!*p)
		return NULL;

	free(*p);
	*p = NULL;
	return NULL;
}

#endif /* UTIL_H */
