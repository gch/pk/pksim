SUBDIRS-y :=

pksim := pksim$(EXE_EXT)

OBJS-$(pksim)-y := \
	loader.c.o \
	main.c.o \
	pk_drug_lib.c.o \
	pk_model.c.o \
	pk_pop_sim.c.o \
	pk_types.c.o \
	random.c.o \
	stats.c.o

BINS-y := $(pksim)

LDFLAGS-$(pksim)-y += -lcsv -lm
