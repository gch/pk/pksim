#ifndef TYPES_H
#define TYPES_H 1

#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct
{
	float x;
	float y;
} v2_t;

typedef union
{
	struct
	{
		float x;
		float y;
		float z;
		float w;
	};
	struct
	{
		v2_t xy;
		v2_t zw;
	};
} v4_t;

typedef union
{
	struct
	{
		float x;
		float y;
		float z;
		float w;
		float a;
		float b;
	};
	struct
	{
		v2_t xy;
		v2_t zw;
		v2_t ab;
	};
	v4_t xyzw;
} v6_t;


static inline v2_t v2_set(float x, float y) {
	v2_t r;

	r.x = x;
	r.y = y;
	return r;
}

static inline v4_t v4_set(float x, float y, float z, float w) {
	v4_t r;

	r.x = x;
	r.y = y;
	r.z = z;
	r.w = w;
	return r;
}

static inline v4_t v4_set_from_2v2(v2_t xy, v2_t zw) {
	v4_t r;

	r.xy = xy;
	r.zw = zw;
	return r;
}

static inline v6_t v6_set(float x, float y, float z, float w, float a, float b) {
	v6_t r;

	r.x = x;
	r.y = y;
	r.z = z;
	r.w = w;
	r.a = a;
	r.b = b;
	return r;
}

static inline v6_t v6_set_from_v4v2(v4_t xyzw, v2_t ab) {
	v6_t r;

	r.xyzw = xyzw;
	r.ab = ab;
	return r;
}

static inline v2_t v2_mul_v2(v2_t a, v2_t b) {
	return v2_set(
		a.x * b.x,
		a.y * b.y);
}

static inline v4_t v4_mul_v4(v4_t a, v4_t b) {
	return v4_set_from_2v2(
		v2_mul_v2(a.xy, b.xy),
		v2_mul_v2(a.zw, b.zw));
}

static inline v6_t v6_mul_v6(v6_t a, v6_t b) {
	return v6_set_from_v4v2(
		v4_mul_v4(a.xyzw, b.xyzw),
		v2_mul_v2(a.ab, b.ab));
}

static inline v2_t v2_exp(v2_t a) {
	return v2_set(
		expf(a.x),
		expf(a.y));
}

static inline v4_t v4_exp(v4_t a) {
	return v4_set_from_2v2(
		v2_exp(a.xy),
		v2_exp(a.zw));
}

static inline v6_t v6_exp(v6_t a) {
	return v6_set_from_v4v2(
		v4_exp(a.xyzw),
		v2_exp(a.ab));
}

#endif /* TYPES_H */
