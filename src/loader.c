#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <csv.h>

#include "loader.h"
#include "pk_types.h"
#include "util.h"

enum loader_error
{
	LOADER_ERROR_NONE = 0,
	LOADER_ERROR_UNKNOWN_DIRECTIVE
};

enum loader_directive
{
	LOADER_DIRECTIVE_UNKNOWN = 0,
	LOADER_DIRECTIVE_PARAMS,
	LOADER_DIRECTIVE_PATIENT,
	LOADER_DIRECTIVE_ADM,
	LOADER_DIRECTIVE_PREL,
	LOADER_DIRECTIVE_LAUNCH
};

struct parse_state
{
	struct loader_context ctx;
	loader_launch_callback_t launch_cb;
	void *launch_cb_arg;
	enum loader_error err;
	enum loader_directive directive;
	size_t field_idx;
};

static int handle_params(struct parse_state *state, const char *str)
{
	struct loader_context *ctx = &state->ctx;

	switch (state->field_idx)
	{
		case 1: ctx->time_count    = strtoul(str, NULL, 10); break;
		case 2: ctx->time_interval = strtof(str, NULL); break;
		case 3: ctx->sim_count     = strtoul(str, NULL, 10); break;
		case 4: ctx->ap.is_eq      = !!strtoul(str, NULL, 10); break;
		default:
			return -1;
	}

	ctx->has_params = true;
	return 0;
}

static int handle_patient(struct parse_state *state, const char *str)
{
	struct loader_context *ctx = &state->ctx;

	switch (state->field_idx)
	{
		case 1: ctx->lname    = str_dup(str); break;
		case 2: ctx->fname    = str_dup(str); break;
		case 3: ctx->weight   = strtof(str, NULL); break;
		case 4: ctx->height   = strtof(str, NULL); break;
		case 5: ctx->bsa      = strtof(str, NULL); break;
		case 6: ctx->cl_creat = strtof(str, NULL); break;
		default:
			return -1;
	}

	ctx->has_patient = true;
	return 0;
}

static int handle_adm(struct parse_state *state, const char *str)
{
	struct loader_context *ctx = &state->ctx;
	pk_adm_params_t *ap = &ctx->ap;
	pk_adm_t *adm;

	if (state->field_idx == 1)
	{
		pk_adm_t *adms = realloc(ap->adms, (ap->adm_count + 1) * sizeof(ap->adms[0]));
		if (!adms)
			return -1;

		ap->adms = adms;
		ap->adm_count++;
	}

	adm = &ap->adms[ap->adm_count - 1];

	switch (state->field_idx)
	{
		case 1: adm->route = pk_adm_route_from_str(str); break;
		case 2: adm->t     = strtof(str, NULL); break;
		case 3: adm->dur   = strtof(str, NULL); break;
		case 4: adm->dose  = strtof(str, NULL); break;
		default:
			return -1;
	}

	ctx->has_adm = true;
	return 0;
}

static int handle_prel(struct parse_state *state, const char *str)
{
	struct loader_context *ctx = &state->ctx;
	pk_prel_t *prel;

	if (state->field_idx == 1)
	{
		pk_prel_t *prels = realloc(ctx->prels, (ctx->prel_count + 1) * sizeof(ctx->prels[0]));
		if (!prels)
			return -1;

		ctx->prels = prels;
		ctx->prel_count++;
	}

	prel = &ctx->prels[ctx->prel_count - 1];

	switch (state->field_idx)
	{
		case 1: prel->t = strtof(str, NULL); break;
		case 2: prel->c = strtof(str, NULL); break;
		default:
			return -1;
	}

	ctx->has_prel = true;
	return 0;
}

static int handle_launch(struct parse_state *state, const char *str)
{
	struct loader_context *ctx = &state->ctx;
	int result;

	(void)str;

	result = state->launch_cb(&state->ctx, state->launch_cb_arg);

	// Params can be kept between sims
	// ctx->has_params = false;

	ctx->has_patient = false;
	free_p(&ctx->lname);
	free_p(&ctx->fname);

	ctx->has_prel = false;
	free_p(&ctx->prels);
	ctx->prel_count = 0;

	ctx->has_adm = false;
	free_p(&ctx->ap.adms);
	ctx->ap.adm_count = 0;

	return result;
}

typedef int (*loader_directive_callback_t)(struct parse_state *state, const char *str);

typedef struct
{
	const char *name;
	loader_directive_callback_t cb;
} loader_directive_handler_t;

static const loader_directive_handler_t directive_handlers[] = {
	[LOADER_DIRECTIVE_UNKNOWN] = { NULL,      NULL },
	[LOADER_DIRECTIVE_PARAMS]  = { "params",  handle_params },
	[LOADER_DIRECTIVE_PATIENT] = { "patient", handle_patient },
	[LOADER_DIRECTIVE_ADM]     = { "adm",     handle_adm },
	[LOADER_DIRECTIVE_PREL]    = { "prel",    handle_prel },
	[LOADER_DIRECTIVE_LAUNCH]  = { "launch",  handle_launch }
};

static inline enum loader_directive loader_directive_from_str(const char *name) {
	for (size_t i = 0; i < ARRAY_SIZE(directive_handlers); i++)
	{
		const loader_directive_handler_t *handler = &directive_handlers[i];
		if (!handler->name || !handler->cb)
			continue;

		if (strcmp(handler->name, name) == 0)
			return i;
	}

	return LOADER_DIRECTIVE_UNKNOWN;
}

static void handle_field(void *str, size_t size, void *state_)
{
	struct parse_state *state = state_;

	(void)size;

	if (state->err != LOADER_ERROR_NONE)
		return;

	if (state->field_idx == 0)
	{
		state->directive = loader_directive_from_str(str);
		if (state->directive == LOADER_DIRECTIVE_UNKNOWN)
		{
			state->err = LOADER_ERROR_UNKNOWN_DIRECTIVE;
			return;
		}
	}
	else
	{
		const loader_directive_handler_t *handler = &directive_handlers[state->directive];

		handler->cb(state, str);
	}

	state->field_idx++;
}

static void handle_record_end(int c, void *state_)
{
	struct parse_state *state = state_;

	(void)c;

	if (state->err != LOADER_ERROR_NONE)
		return;

	state->directive = LOADER_DIRECTIVE_UNKNOWN;
	state->field_idx = 0;
}

int loader_process_file(FILE *fp, loader_launch_callback_t launch_cb, void *launch_cb_arg)
{
	struct parse_state state;
	struct csv_parser parser;

	memset(&state, 0, sizeof(state));
	state.launch_cb = launch_cb;
	state.launch_cb_arg = launch_cb_arg;
	state.err = LOADER_ERROR_NONE;
	state.directive = LOADER_DIRECTIVE_UNKNOWN;
	state.field_idx = 0;

	csv_init(&parser, CSV_APPEND_NULL);
	csv_set_delim(&parser, CSV_TAB);

	while (!feof(fp))
	{
		char data[1024];
		size_t size;
		size_t proc_size;

		size = fread(data, 1, sizeof(data), fp);
		if (ferror(fp))
			goto fail_read;

		proc_size = csv_parse(&parser, data, size, handle_field, handle_record_end, &state);
		if (proc_size != size)
			goto fail_parse;
	}

	csv_fini(&parser, handle_field, handle_record_end, &state);
	csv_free(&parser);

	return 0;

fail_parse:
fail_read:
	return -1;
}
