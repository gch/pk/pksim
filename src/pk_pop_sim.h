#ifndef PK_POP_SIM_H
#define PK_POP_SIM_H 1

#include "pk_types.h"

extern void pk_pop_sim_conc(size_t sim_count, float cs[], const pk_model_params_t *model, const pk_adm_params_t *ap, float t);

#endif /* PK_POP_SIM_H */
