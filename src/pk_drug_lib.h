#ifndef PK_DRUG_LIB_H
#define PK_DRUG_LIB_H 1

#include "pk_types.h"

extern void pk_compute_model_params(enum pk_drug drug, pk_model_params_t *model, const pk_covar_t *covar);

#endif /* PK_DRUG_LIB_H */
