#include "pk_model.h"
#include "pk_pop_sim.h"
#include "pk_types.h"
#include "random.h"

void pk_pop_sim_conc(size_t sim_count, float cs[], const pk_model_params_t *model, const pk_adm_params_t *ap, float t)
{
	for (size_t i = 0; i < sim_count; i++)
	{
		float c = pk_model_compute_conc(model, ap, t);
		float eps = (model->res_model.x + model->res_model.y * c) * rand_normal_unsafe();
		cs[i] = c + eps;
	}
}
