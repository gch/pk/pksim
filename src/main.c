#include <stdio.h>
#include <stdlib.h>

#include "loader.h"
#include "pk_drug_lib.h"
#include "pk_model.h"
#include "pk_pop_sim.h"
#include "pk_types.h"
#include "random.h"
#include "stats.h"
#include "util.h"

static inline float bsa_boyd(float weight_kg, float height_cm)
{
	float weight_g = weight_kg * 1000.f;
	return .0001f * 3.207f * powf(weight_g, .7285f - .0188f * log10f(weight_g)) * powf(height_cm, .3f);
}

struct main_context
{
	const char *out_prefix;
};

static int handle_patient_data(struct loader_context *ctx, void *arg)
{
	struct main_context *mctx = arg;
	pk_adm_params_t *ap = &ctx->ap;
	const size_t patient_count = 1;
	int status = -1;
	char filename[1024];
	FILE *fp;
	float *cs;
	pk_covar_t covar;
	pk_model_params_t model;

	if (!ctx->has_params
			|| !ctx->has_patient
			|| !ctx->has_adm
			|| !ctx->has_prel)
		return -1;

	snprintf(filename, sizeof(filename), "%s%s-%s.tsv", mctx->out_prefix, ctx->lname, ctx->fname);

	fprintf(stderr, "Generating %s...\n", filename);

	fp = fopen(filename, "w");
	if (!fp)
		goto fail_open;

	cs = malloc(sizeof(cs[0]) * ctx->sim_count);
	if (!cs)
		goto fail_alloc;

	covar.bsa = ctx->bsa;
	covar.cl_creat = ctx->cl_creat;
	pk_compute_model_params(PK_DRUG_DALBA, &model, &covar);

	fprintf(fp, "Time (h)\tQ05 (mg/L)\tQ50 (mg/L)\tQ95 (mg/L)"
			"\t\tPatient last name\tPatient first name\tPatient weight (kg)\tPatient height (cm)\tPatient BSA (m^2)\tPatient CLcreat (mL/min)"
			"\t\tAdmin route\tAdmin time (h)\tAdmin duration (h)\tAdmin dose (mg)"
			"\t\tSample time (t)\tSample concentration (mg/L)\n");

	for (size_t i = 0; i < ctx->time_count || i < patient_count || i < ap->adm_count || i < ctx->prel_count; i++)
	{
		if (i < ctx->time_count)
		{
			float t = (float)i;
			float q05;
			float q50;
			float q95;

			pk_pop_sim_conc(ctx->sim_count, cs, &model, ap, t);

			stats_samples_sort_asc(ctx->sim_count, cs);
			q05 = stats_quantile_r7(.05f, ctx->sim_count, cs);
			q50 = stats_quantile_r7(.50f, ctx->sim_count, cs);
			q95 = stats_quantile_r7(.95f, ctx->sim_count, cs);

			fprintf(fp, "%f\t%f\t%f\t%f", t, q05, q50, q95);
		}
		else
			fprintf(fp, "\t\t\t");

		if (i < patient_count)
			fprintf(fp, "\t\t\"%s\"\t\"%s\"\t%f\t%f\t%f\t%f", ctx->lname, ctx->fname, ctx->weight, ctx->height, ctx->bsa, ctx->cl_creat);
		else
			fprintf(fp, "\t\t\t\t\t\t\t");

		if (i < ap->adm_count)
			fprintf(fp, "\t\t\"%s\"\t%f\t%f\t%f", pk_adm_route_to_str(ap->adms[i].route), ap->adms[i].t, ap->adms[i].dur, ap->adms[i].dose);
		else
			fprintf(fp, "\t\t\t\t\t");

		if (i < ctx->prel_count)
			fprintf(fp, "\t\t%f\t%f", ctx->prels[i].t, ctx->prels[i].c);
		else
			fprintf(fp, "\t\t\t");

		fprintf(fp, "\n");
	}

	status = 0;

	free(cs);

fail_alloc:
	fclose(fp);

fail_open:
	return status;
}

int main(int argc, char **argv)
{
	struct main_context mctx;
	const char *filename;
	FILE *fp;

	if (argc < 2 || argc > 3)
	{
		fprintf(stderr, "Usage: %s <data.tsv> [output prefix]\n", argc >= 1 ? argv[0] : "pksim");
		return 1;
	}

	filename = argv[1];
	mctx.out_prefix = argc >= 3 ? argv[2] : "";

	fp = fopen(filename, "rb");
	if (!fp)
	{
		fprintf(stderr, "ERR: Failed to open input file \"%s\"\n", filename);
		return 1;
	}

	rand_init();
	loader_process_file(fp, handle_patient_data, &mctx);

	fclose(fp);
	return 0;
}
