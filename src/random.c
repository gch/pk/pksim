#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "random.h"
#include "types.h"
#include "util.h"

void rand_init(void)
{
	srand(time(NULL));
}

void prng_squares_init(struct prng_squares_ctx *ctx)
{
	uint64_t seed = UINT64_C(0xc12d5aea9d4e8153);
	uint64_t r = ((uint64_t)rand() << 32) ^ rand();

	seed ^= UINT64_C(0x9742defdcb71ef4d) ^ r ^ time(NULL);

	prng_squares_init_seed(ctx, seed);
}

void prng_squares_init_seed(struct prng_squares_ctx *ctx, uint64_t seed)
{
	static const uint64_t keys[] = {
		0xac4de7fcb148fd67,
		0x234f68acbf5a1cd3,
		0x59b278d1fd6e3c51,
		0x978bc3532e5149ad,
		0x84fd75b43d54692b,
		0xe258ac954c356897,
		0xe82cf4d7684a97f3,
		0xc9753deba84db671,
		0x5fc1a7ea953fa4cd,
		0x4f6187bed562e34b,
		0xb89e3c4873b584a9,
		0x2938db1dc3a9b417,
		0xf178eb4ed19bc283,
		0x2a6d357a9e6cb1df,
		0xeabf9436576b4aef,
		0x6572ebaa947f5a6d
	};

	ctx->seed = seed;
	ctx->key = keys[seed % ARRAY_SIZE(keys)];
	ctx->ctr = seed ^ UINT64_C(0xbcd874165b7a4d93);
}

void prng_squares_init_key_ctr(struct prng_squares_ctx *ctx, uint64_t key, uint64_t ctr)
{
	ctx->seed = 0;
	ctx->key = key;
	ctx->ctr = ctr;
}

uint32_t prng_squares(struct prng_squares_ctx *ctx)
{
	uint64_t key = ctx->key;
	uint64_t ctr = ctx->ctr++;
	uint64_t x;
	uint64_t y;
	uint64_t z;

	x = y = ctr * key;
	z = y + key;

	x = x * x + y;
	x = (x >> 32) | (x << 32);

	x = x * x + z;
	x = (x >> 32) | (x << 32);

	x = x * x + y;
	x = (x >> 32) | (x << 32);

	return (x * x + z) >> 32;
}

uint32_t prng_squares_unsafe(void)
{
	static int is_init = 0;
	static struct prng_squares_ctx ctx;

	if (!is_init)
	{
		prng_squares_init(&ctx);
		is_init = 1;
	}

	return prng_squares(&ctx);
}

#if defined(__STDC_IEC_559__) || defined(__GCC_IEC_559)

/* Resolution: 2^23 distinct values inside [0.f, 1.f[ */
static inline float randf_norm_ieee754_unsafe(void)
{
	uint32_t r = prng_squares_unsafe();
	r = ((0 + 127) << 23) | (r & 0x7FFFFFul);

	/* 1.f <= r < 2.f */
	char *pr = (char *)&r;
	return *(float *)pr - 1.f;
}

/* Resolution: 2^23 distinct values inside [-1.f, 1.f[ */
static inline float randf_snorm_ieee754_unsafe(void)
{
	uint32_t r = prng_squares_unsafe();
	r = ((1 + 127) << 23) | (r & 0x7FFFFFul);

	/* 2.f <= r < 4.f */
	char *pr = (char *)&r;
	return *(float *)pr - 3.f;
}

#endif

static inline float randf_norm_dumb_unsafe(void)
{
	float irm = 2.f / ((uint64_t)UINT32_MAX + 1u);
	uint32_t r = prng_squares_unsafe();

	return irm * r;
}

static inline float randf_snorm_dumb_unsafe(void)
{
	float irm = 2.f / ((uint64_t)UINT32_MAX + 1u);
	uint32_t r = prng_squares_unsafe();

	return irm * r - 1.f;
}

/* Rand value inside [0.f, 1.f[ */
float randf_norm_unsafe(void)
{
#if defined(__STDC_IEC_559__) || defined(__GCC_IEC_559__)
	return randf_norm_ieee754_unsafe();
#else
	return randf_norm_dumb_unsafe();
#endif
}

/* Rand value inside [-1.f, 1.f[ */
float randf_snorm(void)
{
#if defined(__STDC_IEC_559__) || defined(__GCC_IEC_559__)
	return randf_snorm_ieee754_unsafe();
#else
	return randf_snorm_dumb_unsafe();
#endif
}

v2_t rand_normal_masaglia_unsafe(void)
{
	v2_t r;
	float u;

	do
	{
		r = v2_set(randf_snorm(), randf_snorm());
		u = r.x * r.x + r.y * r.y;
	}
	while (u == 0.f || u >= 1.f);

	u = sqrtf(-2.f * logf(u) / u);
	r.x *= u;
	r.y *= u;

	return r;
}

float rand_normal_unsafe(void)
{
	return rand_normal_masaglia_unsafe().x;
}

v2_t v2_rand_normal_unsafe(void)
{
	return rand_normal_masaglia_unsafe();
}

v4_t v4_rand_normal_unsafe(void)
{
	return v4_set_from_2v2(
			v2_rand_normal_unsafe(),
			v2_rand_normal_unsafe());
}

v6_t v6_rand_normal_unsafe(void)
{
	return v6_set_from_v4v2(
			v4_rand_normal_unsafe(),
			v2_rand_normal_unsafe());
}
